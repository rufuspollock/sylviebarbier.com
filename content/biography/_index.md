---
title: Biography
---

Sylvie Shiwei Barbier is a French - Taiwanese artist, who works primarily with the body as a medium in the form of performance art, photography and videos. She investigate human condition through the thematics of birth, death, joy, memory, longing, family & instinct. The body is the language of her artistic universe where she bridge east and west aesthetics. Sylvie is the co-founder of Art / Earth / Tech a community oriented initiatives based in France, UK & Taiwan. She facilitate collective intelligence workshop using performance art techniques to enable authentic conversation with various groups. 