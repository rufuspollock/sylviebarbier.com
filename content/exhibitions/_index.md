---
title: List of Exhibitions
---

* 2018
  * ‘Words of wisdom II’ Art’s Birth Day (London)

* 2017
  * ‘Collective Performance Opera’ Venice International
 Performance Art Week (Venice)
  * ‘Malaise’ Performance (Paris)
  * ‘Voir’, Aie Gallery (Taipei)

* 2016
  * ‘Non Attachment to views’ (London)

* 2015
  * ‘First Instinct’ Indie Fest Award (US)
  * ‘First Instinct’ Premiere (Florence)
  * ‘V’ (Florence)
  
* 2014  					
  * ‘Dune’ Bjork (Florence) 
  * ‘Burning Chianti’ (Florence)
  * ‘Dune’ at Young Art Taipei’ (Taipei)
  					
* 2013  					
  *  ‘Dormi Veglia’ (Florence)
  * ‘Manfredi Lifestyle’  Home experience. (Florence)

* 2012
  					
  * ‘Art Taipei’ Star Crystal Gallery (Taipei)
  * ‘International Art Forum’ Speaker at the World youth Alliance (New York)
  * ‘Art for Youth’ Royal College of Art (London)
  * ‘I.M.I show’ Bussey Building (London)
  * ‘Bright Young Things’ She has a Space (London)
  * ‘Words of Wisdom’ Kingly Court (London)
  					
* 2011
  					
  * ‘I.M.I interim show’ Well Gallery (London)
  * ‘Art Taipei 2011’ Star Crystal Gallery (Taipei)
  * ‘Young Art Taipei 2011’ Star Crystal Gallery (Taipei)
  					
* 2010
  					
  * Speaker at at the ‘International Conference of Propagation and Implementation of the Idea of Human Rights’ (Taipei)
  					
* 2009
  					
  * ‘Changing face of Britain ’ Supper Club (London)
  * December ‘Changing face of Britain’ Well Gallery (London)

</br>

## Publications:

* Open House magazine issue 8 on ‘Art / Earth / Tech’ (2017)
* ‘Voir’ catalogue (2017)
* Radio interview on Resonance 104.4 Taiwan RE on ‘Art / Earth / Tech’ (2015)
* New York Times ‘Intersection’ (2014)
* ‘Dune’ book (2013)
* Varrom Magazine ‘Five things I learned at the LCC show’ (2012)
* Radio interview on Resonance 104.4 Taiwan RE on ‘Words of Wisdom’ (2012)
* Voice of Photography Magazine ‘Rorschach’ (2012)
* Art Collection + Design ‘Art Fair, Young Art Taipei’ (2011)
* Artitude ‘YAT 2011’ (2011)
* Less Common more sense, ‘The Great British Issue’ (2010)
* Arts London News ‘The Changing Face of Britain’ (2009)
* Art of Life ‘Emotion in Motion’ (2009)
