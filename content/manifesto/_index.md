---
title: Manifesto
---

## ART AND TOLERANCE


In a world that is becoming more globalized, strong divisions and conflicts are emerging, partially due to a polarized view of others.
Can art contribute to the reconnection between human beings?
If this is possible, messages of tolerance, compassion, and empathy are essential to bring people closer to one another. Indeed, by revealing the universality of our behaviour and condition, each individual becomes equal, deserving of respect and of not being judged. Demonstrating empathy by means of art is an invitation for the other to understand different perspectives, even if their beliefs are being threatened. Through aesthetic, the beauty of humanity as a unified family can be exposed to the human eye and make our view of one another less frightening. Closed into our beliefs as we identify ourselves in them, we are blinded to what links us as a civilization and to the universe. Art could take a central position in promoting compassion and tolerance. 
To accept ourselves for who we are, to not expect more than one can give, to acknowledge our imperfections, enrich ourselves to alternatives, enhance others in their potential and perhaps even to tolerate intolerance. All this will contribute towards and sustain human progress.

## THE SUBLIME

Where is the sublime? Where is our sense of awe? Seeking to touch what goes beyond us is the story of humanity;, through the sciences, technology, philosophy or art we can find the sublime because we hold it within ourselves. We can only see it because it is in us and it is in all that surround us. Touched, moved in our core where we face our universal human condition and at the moment where we almost perceive the divine. This sense is a gift and it lifts us from our sorrow. We stand for an art that elevates the spirit, created to serve humanity and illuminate the soul. There has been a loss of those seeking the sublime in art and with that we have lost the sense that art is the craft of creating sacredness. 

We call upon all people and artists for whom these values echo in their hearts to voice their belonging to these values so that we no longer experience disconnection and loneliness when perceiving art. So that we have an art that is for the many and not the few. So that we have an art that speaks to what make us human in the most profound way.

