$(document).ready(function(){
  $(".lazy").not('.slick-initialized').slick({
    infinite: true,
    lazyLoad: 'ondemand',
    mobileFirst: true,
    appendArrows: $('.slider'),
    slidesToShow: 1
  })
})

$('.slider').on('afterChange', function(slick, currentSlide) {
  var categories = document.getElementsByClassName('category');
  for (var i = 0; i < categories.length; i++) {
    categories[i].style = 'font-weight:normal;';
  }
  switch(true) {
    case (currentSlide.currentSlide < 1):
      categories[0].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 10):
      categories[1].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 19):
      categories[2].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 25):
      categories[3].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 29):
      categories[4].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 42):
      categories[5].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 51):
      categories[6].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 67):
      categories[7].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 68):
      categories[9].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 69):
      categories[10].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 70):
      categories[11].style = 'font-weight:bold;';
      break;
    case (currentSlide.currentSlide < 71):
      categories[12].style = 'font-weight:bold;';
      break;

  }
})

const idToText = [
  {"id": "1", "text": "dream-of-birth"},
  {"id": "10", "text": "hybrid-instinct"},
  {"id": "19", "text": "rorschach"},
  {"id": "25", "text": "rorschach-emotions"},
  {"id": "29", "text": "dune"},
  {"id": "42", "text": "voir"},
  {"id": "51", "text": "performance"},
  {"id": "67", "text": "video-first-instinct"},
  {"id": "68", "text": "video-dream-of-birth"},
  {"id": "69", "text": "video-rorschach-emotions"},
  {"id": "70", "text": "video-dune"}
]

$(".slider").each(function(k, v){
    var search = window.location.search,
        $slider = $(this),
        query = search.slice(1),
        initialSlide = 0;
    
    idToText.forEach(slideId => {
      if (slideId.text === query) {
        initialSlide = parseInt(slideId.id);
      }
    })
    $slider.slick({
        initialSlide: initialSlide
    });
});
